# Codename MackerelPad 

A Macropad, Dashboard, Indicator dream


# Objectives

The sentimental objectives are really to advance microcontroller, microelectronics, and related skills as well as make use of my 3D printer and spare parts laying around the house.  


# Features

```mermaid
mindmap
  root((MackerlPad))
    Hardware & Interfaces
        Wireless
            USB HID Interface
            WIFI
            ZIGBEE 
        Wired
            Single Cable
                DC Power Cord + Bluetooth
                USB computer powered
                    Ideally USB C
        HomeKit/HomeAssistant Controllable
            LEDs
        Touchscreen
    Dashboard
        Widgets
        Alerts
        RestAPI
    Macro Pad
        Standard Keys style
        Screen Key style
        Pedal Buttons (foot)
        Rotary Knobs 
            Fusion360
            Volume
            Scrubbing
    Status Indicators
        Single LED toggle
        RGB Color Change & Toggle
        RGB Strip Controller
    Software
        REST Server
            Configs
            Data Updates
            Indicator Controller
        HID Interface for Keys
            Keymap / Key Combo
            Driverless
```

# Inspiration

- [Alex @ Super Make Something](https://www.youtube.com/watch?v=_NqYtvLnY5k&pp=ygUPZGl5IHN0cmVhbSBkZWNr)
- [Elgato Stream Deck MK.2](https://www.elgato.com/us/en/p/stream-deck-mk2-black)
- [Elgato Stream Deck Pedal](https://www.youtube.com/@SmartHomeSolver)
- [Smart Home Solver](https://www.youtube.com/@SmartHomeSolver)
- [Raspberry Pi Smart Mirrors](https://www.youtube.com/results?search_query=smart+mirror)
- [XENCELABS Quick Keys](https://www.amazon.com/gp/product/B08VNCLDTK/ref=ox_sc_saved_image_9?smid=A2AO8XL38SU3ST&psc=1)
